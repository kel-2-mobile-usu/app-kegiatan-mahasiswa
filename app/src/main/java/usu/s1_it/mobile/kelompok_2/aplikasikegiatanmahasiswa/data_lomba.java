package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/4/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class data_lomba {

    @SerializedName("id_lomba")
    @Expose
    private String idLomba;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tema")
    @Expose
    private String tema;
    @SerializedName("jenis")
    @Expose
    private String jenis;
    @SerializedName("jadwal")
    @Expose
    private String jadwal;
    @SerializedName("mulai")
    @Expose
    private String mulai;
    @SerializedName("selesai")
    @Expose
    private String selesai;
    @SerializedName("biaya")
    @Expose
    private String biaya;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("lokasi")
    @Expose
    private String lokasi;
    @SerializedName("brosur")
    @Expose
    private String brosur;
    @SerializedName("cp")
    @Expose
    private String cp;
    @SerializedName("penyelenggara")
    @Expose
    private String penyelenggara;

    /**
     *
     * @return
     * The idLomba
     */
    public String getIdLomba() {
        return idLomba;
    }

    /**
     *
     * @param idLomba
     * The id_lomba
     */
    public void setIdLomba(String idLomba) {
        this.idLomba = idLomba;
    }

    /**
     *
     * @return
     * The nama
     */
    public String getNama() {
        return nama;
    }

    /**
     *
     * @param nama
     * The nama
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     *
     * @return
     * The tema
     */
    public String getTema() {
        return tema;
    }

    /**
     *
     * @param tema
     * The tema
     */
    public void setTema(String tema) {
        this.tema = tema;
    }

    /**
     *
     * @return
     * The jenis
     */
    public String getJenis() {
        return jenis;
    }

    /**
     *
     * @param jenis
     * The jenis
     */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    /**
     *
     * @return
     * The jadwal
     */
    public String getJadwal() {
        return jadwal;
    }

    /**
     *
     * @param jadwal
     * The jadwal
     */
    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    /**
     *
     * @return
     * The mulai
     */
    public String getMulai() {
        return mulai;
    }

    /**
     *
     * @param mulai
     * The mulai
     */
    public void setMulai(String mulai) {
        this.mulai = mulai;
    }

    /**
     *
     * @return
     * The selesai
     */
    public String getSelesai() {
        return selesai;
    }

    /**
     *
     * @param selesai
     * The selesai
     */
    public void setSelesai(String selesai) {
        this.selesai = selesai;
    }

    /**
     *
     * @return
     * The biaya
     */
    public String getBiaya() {
        return biaya;
    }

    /**
     *
     * @param biaya
     * The biaya
     */
    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The lokasi
     */
    public String getLokasi() {
        return lokasi;
    }

    /**
     *
     * @param lokasi
     * The lokasi
     */
    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    /**
     *
     * @return
     * The brosur
     */
    public String getBrosur() {
        return brosur;
    }

    /**
     *
     * @param brosur
     * The brosur
     */
    public void setBrosur(String brosur) {
        this.brosur = brosur;
    }

    /**
     *
     * @return
     * The cp
     */
    public String getCp() {
        return cp;
    }

    /**
     *
     * @param cp
     * The cp
     */
    public void setCp(String cp) {
        this.cp = cp;
    }

    /**
     *
     * @return
     * The penyelenggara
     */
    public String getPenyelenggara() {
        return penyelenggara;
    }

    /**
     *
     * @param penyelenggara
     * The penyelenggara
     */
    public void setPenyelenggara(String penyelenggara) {
        this.penyelenggara = penyelenggara;
    }


}