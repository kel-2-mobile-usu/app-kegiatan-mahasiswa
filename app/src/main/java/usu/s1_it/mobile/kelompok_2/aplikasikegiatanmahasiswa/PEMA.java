package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PEMA extends AppCompatActivity {

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/";

    ListView List;
    public PEMA() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pem);
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        List = (ListView) findViewById(R.id.list_pema);
        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);
        api.getpema(new Callback<datapema>() {
            @Override
            public void success(datapema data, Response response) {

                ListAdapter adapter = new CustomAdapter_pema(PEMA.this, data.getDataPema()
                );

                List.setAdapter(adapter);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ukm", error.getMessage());
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pema, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
