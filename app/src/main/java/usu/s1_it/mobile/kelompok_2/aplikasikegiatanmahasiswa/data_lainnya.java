package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/12/2016.
 */


    import com.google.gson.annotations.Expose;
    import com.google.gson.annotations.SerializedName;


    public class data_lainnya {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("judul")
        @Expose
        private String judul;
        @SerializedName("tanggal")
        @Expose
        private String tanggal;
        @SerializedName("lokasi")
        @Expose
        private String lokasi;
        @SerializedName("jam")
        @Expose
        private String jam;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("brosur")
        @Expose
        private String brosur;
        @SerializedName("penyelenggara")
        @Expose
        private String penyelenggara;
        @SerializedName("cp")
        @Expose
        private String cp;

        /**
         *
         * @return
         * The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The judul
         */
        public String getJudul() {
            return judul;
        }

        /**
         *
         * @param judul
         * The judul
         */
        public void setJudul(String judul) {
            this.judul = judul;
        }

        /**
         *
         * @return
         * The tanggal
         */
        public String getTanggal() {
            return tanggal;
        }

        /**
         *
         * @param tanggal
         * The tanggal
         */
        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        /**
         *
         * @return
         * The lokasi
         */
        public String getLokasi() {
            return lokasi;
        }

        /**
         *
         * @param lokasi
         * The lokasi
         */
        public void setLokasi(String lokasi) {
            this.lokasi = lokasi;
        }


        /**
         *
         * @return
         * The jam
         */
        public String getJam() {
            return jam;
        }

        /**
         *
         * @param jam
         * The jam
         */
        public void setJam(String jam) {
            this.jam = jam;
        }

        /**
         *
         * @return
         * The deskripsi
         */
        public String getDeskripsi() {
            return deskripsi;
        }

        /**
         *
         * @param deskripsi
         * The deskripsi
         */
        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        /**
         *
         * @return
         * The brosur
         */
        public String getBrosur() {
            return brosur;
        }

        /**
         *
         * @param brosur
         * The brosur
         */
        public void setBrosur(String brosur) {
            this.brosur = brosur;
        }

        /**
         *
         * @return
         * The penyelenggara
         */
        public String getPenyelenggara() {
            return penyelenggara;
        }

        /**
         *
         * @param penyelenggara
         * The penyelenggara
         */
        public void setPenyelenggara(String penyelenggara) {
            this.penyelenggara = penyelenggara;
        }

        /**
         *
         * @return
         * The cp
         */
        public String getCp() {
            return cp;
        }

        /**
         *
         * @param cp
         * The cp
         */
        public void setCp(String cp) {
            this.cp = cp;
        }

    }

