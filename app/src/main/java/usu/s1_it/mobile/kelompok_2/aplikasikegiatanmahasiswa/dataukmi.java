package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 14-01-16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dataukmi {

    @SerializedName("data_ukmi")
    @Expose
    private List<data_ukmi> dataUkmi = new ArrayList<data_ukmi>();

    /**
     *
     * @return
     * The dataUkmi
     */
    public List<data_ukmi> getDataUkmi() {
        return dataUkmi;
    }

    /**
     *
     * @param dataUkmi
     * The data_ukmi
     */
    public void setDataUkmi(List<data_ukmi> dataUkmi) {
        this.dataUkmi = dataUkmi;
    }

}