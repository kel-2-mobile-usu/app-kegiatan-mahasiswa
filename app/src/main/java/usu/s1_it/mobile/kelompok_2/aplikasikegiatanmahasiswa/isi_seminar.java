package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.util.Linkify;
import com.squareup.picasso.Picasso;

public class isi_seminar extends AppCompatActivity {
    String PATH = "http://ukm-mobile.esy.es/images/seminar/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_seminar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView Header = (TextView) findViewById(R.id.header);
        ImageView Gambar = (ImageView) findViewById(R.id.imageSeminar);
        TextView Tema = (TextView) findViewById(R.id.texttema2);
        TextView Tanggal = (TextView) findViewById(R.id.texttgl2);
        TextView Jam = (TextView) findViewById(R.id.textjam2);
        TextView Lokasi = (TextView) findViewById(R.id.textlokasi2);
        TextView Pembicara = (TextView) findViewById(R.id.textpembicara2);

        TextView Link = (TextView) findViewById(R.id.textlink2);
        Linkify.addLinks(Link, Linkify.WEB_URLS);
        TextView Biaya = (TextView) findViewById(R.id.textbiaya2);
        TextView Jenis = (TextView) findViewById(R.id.textjenis2);
        TextView Contact = (TextView) findViewById(R.id.textcp2);
        Intent intent = getIntent();
        Header.setText(intent.getStringExtra("Judul"));
        Picasso.with(this).load(PATH + intent.getStringExtra("Gambar")).into(Gambar);
        Tema.setText(intent.getStringExtra("Tema"));
        Tanggal.setText(intent.getStringExtra("Tanggal"));
        Jam.setText(intent.getStringExtra("Jam"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Pembicara.setText(intent.getStringExtra("Pembicara"));
        Link.setText(intent.getStringExtra("Link"));
        Biaya.setText(intent.getStringExtra("Biaya"));
        Jenis.setText(intent.getStringExtra("Jenis"));
        Contact.setText(intent.getStringExtra("Contact Person"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_isi_seminar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}