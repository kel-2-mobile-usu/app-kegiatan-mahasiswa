package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 14-01-16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dataimilkom {

    @SerializedName("data_imilkom")
    @Expose
    private List<data_imilkom> dataImilkom = new ArrayList<data_imilkom>();

    /**
     *
     * @return
     * The dataImilkom
     */
    public List<data_imilkom> getDataImilkom() {
        return dataImilkom;
    }

    /**
     *
     * @param dataImilkom
     * The data_imilkom
     */
    public void setDataImilkom(List<data_imilkom> dataImilkom) {
        this.dataImilkom = dataImilkom;
    }

}
