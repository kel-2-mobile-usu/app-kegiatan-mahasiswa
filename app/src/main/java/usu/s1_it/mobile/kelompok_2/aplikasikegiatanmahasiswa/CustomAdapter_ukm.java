package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class CustomAdapter_ukm extends ArrayAdapter<data_ukm> {

    String PATH = "http://ukm-mobile.esy.es/images/ukm/";
    public CustomAdapter_ukm(Context context, List<data_ukm> data) {
        super(context, R.layout.activity_row_ukm, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_ukm, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdlukm);
        ImageView gambar = (ImageView) customView.findViewById(R.id.imageViewukm);

        data_ukm dataukm = getItem(position);

        judul.setText(dataukm.getJudul());

        Picasso.with(getContext()).load(PATH+dataukm.getLogo()).resize(100,100).into(gambar);

        return customView;
    }
}
