package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Asus on 17/01/2016.
 */
public class CustomAdapter_EditLomba extends ArrayAdapter<data_lomba> {

    String PATH = "http://ukm-mobile.esy.es/images/lomba/";
    public CustomAdapter_EditLomba(Context context, List<data_lomba> data) {
        super(context, R.layout.activity_row_edit_lomba, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_edit_lomba, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdllomba);
        TextView jadwal = (TextView) customView.findViewById(R.id.textjadwal);
        ImageView gambar = (ImageView) customView.findViewById(R.id.imageViewlomba);

        data_lomba datalomba = getItem(position);

        judul.setText(datalomba.getNama());
        jadwal.setText(datalomba.getJadwal());


        Picasso.with(getContext()).load(PATH+datalomba.getBrosur()).resize(70,100).into(gambar);



        return customView;
    }
}
