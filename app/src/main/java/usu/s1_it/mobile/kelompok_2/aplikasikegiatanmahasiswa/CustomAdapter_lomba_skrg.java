package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class CustomAdapter_lomba_skrg extends ArrayAdapter<data_lomba> {

    String PATH = "http://ukm-mobile.esy.es/images/lomba/";
    public CustomAdapter_lomba_skrg(Context context, List<data_lomba> data) {
        super(context, R.layout.activity_row_lomba_skrg, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_lomba_skrg, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdllombas);
        TextView lokasi = (TextView) customView.findViewById(R.id.textlokasilombas);
        ImageView gambar = (ImageView) customView.findViewById(R.id.imagelombaskrg);

        data_lomba datalomba = getItem(position);

        judul.setText(datalomba.getNama());
        lokasi.setText(datalomba.getLokasi());


        Picasso.with(getContext()).load(PATH+datalomba.getBrosur()).resize(70,100).into(gambar);



        return customView;
    }
}
