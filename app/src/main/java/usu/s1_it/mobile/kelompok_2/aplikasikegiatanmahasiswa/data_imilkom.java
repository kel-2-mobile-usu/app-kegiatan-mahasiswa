package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class data_imilkom {

    @SerializedName("judul")
    @Expose
    private String judul;

    /**
     *
     * @return
     * The judul
     */
    public String getJudul() {
        return judul;
    }

    /**
     *
     * @param judul
     * The judul
     */
    public void setJudul(String judul) {
        this.judul = judul;
    }

}
