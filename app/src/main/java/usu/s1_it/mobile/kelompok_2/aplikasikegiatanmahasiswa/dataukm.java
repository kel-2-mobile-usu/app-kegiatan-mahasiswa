package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/4/2016.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dataukm {

    @SerializedName("data_ukm")
    @Expose
    public List<data_ukm> dataUkm = new ArrayList<data_ukm>();

    /**
     *
     * @return
     * The dataUkm
     */
    public List<data_ukm> getDataUkm() {
        return dataUkm;
    }

    /**
     *
     * @param dataUkm
     * The data_ukm
     */
    public void setDataUkm(List<data_ukm> dataUkm) {
        this.dataUkm = dataUkm;
    }

}
