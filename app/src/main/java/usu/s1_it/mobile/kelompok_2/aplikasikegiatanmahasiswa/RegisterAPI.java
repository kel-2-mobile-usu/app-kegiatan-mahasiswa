package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface RegisterAPI {
    @FormUrlEncoded
    @POST("/insert.php")
    //@POST("/RetrofitExample/insert.php")

    public void insertUser(
            @Field("username") String username,
            @Field("password") String password,
            Callback<Response> callback);

    @Multipart
    @POST("/insert_ukm.php")
    public void insertUKM(
            @Part("judul") String judul,
            @Part("image") TypedFile file,
            @Part("deskripsi") String deskripsi,
            Callback<Response> response);

    @Multipart
    @POST("/insert_seminar.php")
    public void insertSeminar(
            @Part("judul") String judul,
            @Part("tema") String tema,
            @Part("tanggal") String tanggal,
            @Part("waktu") String waktu,
            @Part("lokasi") String lokasi,
            @Part("pembicara") String pembicara,
            @Part("link") String link,
            @Part("biaya") String biaya,
            @Part("jenis") String jenis,
            @Part("image") TypedFile file,
            @Part("cp") String cp,
            @Part("penyelenggara") String penyelenggara,
            Callback<Response> response);

    @Multipart
    @POST("/insert_lainnya.php")
    public void insertLainnya(
            @Part("judul") String judul,
            @Part("tanggal") String tanggal,
            @Part("lokasi") String lokasi,
            @Part("jam") String jam,
            @Part("deskripsi") String deskripsi,
            @Part("brosur") TypedFile file,
            @Part("penyelenggara") String penyelenggara,
            @Part("cp") String cp,
            Callback<Response> response);

    @Multipart
    @POST("/insert_fotoseminar.php")
    public void insertFotoSeminar(
            @Part("Id") String Id,
            @Part("image") TypedFile file,
            Callback<Response> response);

    @Multipart
    @POST("/insert_lomba.php")
    public void insertLomba(
            @Part("nama") String nama,
            @Part("tema") String tema,
            @Part("jenis") String jenis,
            @Part("jadwal") String jadwal,
            @Part("mulai") String mulai,
            @Part("selesai") String selesai,
            @Part("biaya") String biaya,
            @Part("link") String link,
            @Part("lokasi") String lokasi,
            @Part("image") TypedFile file,
            @Part("cp") String cp,
            @Part("penyelenggara") String penyelenggara,
            Callback<Response> response);

    @GET("/login.php")
    public void login(
            @Query("username") String username,
            @Query("password") String password,
            Callback<User> response);

    @GET("/data_seminar.php")
    public void getseminar(Callback<dataseminar> response);

    @GET("/data_lainnya.php")
    public void getlainnya(Callback<datalainnya> response);

    @GET("/data_lainnya_skrg.php")
    public void getlainnya_skrg(Callback<datalainnya> response);

    @GET("/data_seminar_skrg.php")
    public void getseminar_skrg(Callback<dataseminar> response);

    @GET("/data_workshop.php")
    public void getworkshop(Callback<dataseminar> response);

    @GET("/data_galeri.php")
    public void getgaleri(Callback<datagaleri> response);

    @GET("/data_workshop_skrg.php")
    public void getworkshop_skrg(Callback<dataseminar> response);

    @GET("/data_ukmlain.php")
    public void getdataukmlain(Callback<dataukmlain> response);

    @GET("/data_imilkom.php")
    public void getimilkom(Callback<dataimilkom> response);

    @GET("/data_himatif.php")
    public void gethimatif(Callback<datahimatif> response);

    @GET("/data_pema.php")
    public void getpema(Callback<datapema> response);

    @GET("/data_ukmi.php")
    public void getukmi(Callback<dataukmi> response);

    @GET("/data_lomba.php")
    public void getlomba(Callback<datalomba> response);

    @GET("/data_lomba_skrg.php")
    public void getlomba_skrg(Callback<datalomba> response);





    @GET("/data_seminar2.php")
    public void getseminar2(Callback<dataseminar> response);

    @GET("/data_lomba2.php")
    public void getlomba2(Callback<datalomba> response);

    @GET("/data_lainnya2.php")
    public void getlainnya2(Callback<datalainnya> response);


    @Multipart
    @POST("/edit_seminar.php")
    public void editSeminar(
            @Part("id_seminar") String id_seminar,
            @Part("judul") String judul,
            @Part("tema") String tema,
            @Part("tanggal") String tanggal,
            @Part("waktu") String waktu,
            @Part("lokasi") String lokasi,
            @Part("pembicara") String pembicara,
            @Part("link") String link,
            @Part("biaya") String biaya,
            @Part("jenis") String jenis,
            @Part("cp") String cp,
            @Part("penyelenggara") String penyelenggara,
            Callback<Response> response);


    @Multipart
    @POST("/edit_lomba.php")
    public void editLomba(
            @Part("id_lomba") String id_lomba,
            @Part("nama") String nama,
            @Part("tema") String tema,
            @Part("jenis") String jenis,
            @Part("jadwal") String jadwal,
            @Part("mulai") String mulai,
            @Part("selesai") String selesai,
            @Part("biaya") String biaya,
            @Part("link") String link,
            @Part("lokasi") String lokasi,
            @Part("cp") String cp,
            @Part("penyelenggara") String penyelenggara,
            Callback<Response> response);

    @Multipart
    @POST("/edit_lainnya.php")
    public void editLainnya(
            @Part("id") String id,
            @Part("judul") String judul,
            @Part("tanggal") String tanggal,
            @Part("lokasi") String lokasi,
            @Part("jam") String jam,
            @Part("deskripsi") String deskripsi,
            @Part("cp") String cp,
            @Part("penyelenggara") String penyelenggara,
            Callback<Response> response);
}
