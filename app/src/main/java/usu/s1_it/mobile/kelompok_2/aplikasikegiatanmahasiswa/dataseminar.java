package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class dataseminar {

    @SerializedName("data_seminar")
    @Expose
    public List<data_seminar> data = new ArrayList<data_seminar>();


    public List<data_seminar> getSeminar() {
        return data;
    }


    public void setSeminar(List<data_seminar> data) {
        this.data = data;
    }
}
