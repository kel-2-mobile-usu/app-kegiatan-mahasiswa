package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class seminar extends Fragment {

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/";
    Activity context;
    ListView List;
    public seminar() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        View view = inflater.inflate(R.layout.fragment_seminar, container, false);
        List = (ListView) view.findViewById(R.id.list_seminar);
                //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);
        api.getseminar(new Callback<dataseminar>() {
            @Override
            public void success(final dataseminar data, Response response) {

                ListAdapter adapter = new CustomAdapter(getActivity(), data.data);
                List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getActivity(), isi_seminar.class);

                        i.putExtra("Judul", data.data.get(position).getJudul());
                        i.putExtra("Gambar", data.data.get(position).getSpanduk());
                        i.putExtra("Tema", data.data.get(position).getTema());
                        i.putExtra("Tanggal", data.data.get(position).getTanggal());
                        i.putExtra("Jam", data.data.get(position).getWaktu());
                        i.putExtra("Lokasi", data.data.get(position).getLokasi());
                        i.putExtra("Pembicara", data.data.get(position).getPembicara());
                        i.putExtra("Link", data.data.get(position).getLink());
                        i.putExtra("Biaya", data.data.get(position).getBiaya());
                        i.putExtra("Jenis", data.data.get(position).getJenis());
                        i.putExtra("Contact Person", data.data.get(position).getCp());


                        startActivity(i);


                    }
                });
                List.setAdapter(adapter);
                context = getActivity();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getBaseContext(), "Gagal memuat data, silakan berpindah tab terlebih dahulu untuk memuat ulang.", Toast.LENGTH_LONG).show();
            }
        });

        return List;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
