package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 14-01-16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class datahimatif {

    @SerializedName("data_himatif")
    @Expose
    private List<data_himatif> dataHimatif = new ArrayList<data_himatif>();

    /**
     *
     * @return
     * The dataHimatif
     */
    public List<data_himatif> getDataHimatif() {
        return dataHimatif;
    }

    /**
     *
     * @param dataHimatif
     * The data_himatif
     */
    public void setDataHimatif(List<data_himatif> dataHimatif) {
        this.dataHimatif = dataHimatif;
    }

}