package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 15-01-16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class datagaleri {

    @SerializedName("data_galeri")
    @Expose
    private List<data_galeri> dataGaleri = new ArrayList<data_galeri>();

    /**
     *
     * @return
     * The dataGaleri
     */
    public List<data_galeri> getDataGaleri() {
        return dataGaleri;
    }

    /**
     *
     * @param dataGaleri
     * The data_galeri
     */
    public void setDataGaleri(List<data_galeri> dataGaleri) {
        this.dataGaleri = dataGaleri;
    }

}