package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class upload_foto extends AppCompatActivity implements View.OnClickListener{

    private Button buttonUpload;
    private static final String TAG = "CallCamera";
    private static final int CAPTURE_IMAGE_ACTIVITY_REQ = 0;

    private static final int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    Integer mark = 0;
    String photoUriUrl ;

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/insert_fotoseminar.php";

    Uri fileUri = null;
    ImageView gambar = null;

    private File getOutputPhotoFile() {

        File directory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getPackageName());

        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e(TAG, "Failed to create storage directory.");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss", Locale.US).format(new Date());

        return new File(directory.getPath() + File.separator + "IMG_"
                + timeStamp + ".jpg");
    }

    String Id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        Id=intent.getStringExtra("Id");
        setContentView(R.layout.activity_upload_foto);
        gambar = (ImageView) findViewById(R.id.gambar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonUpload = (Button) findViewById(R.id.buttonUpload);

        buttonUpload.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        insertFotoSeminar();
    }

    public void onClickChooseImage(View view) {

        final CharSequence[] options = {"Ambil Foto", "Pilih dari Galeri", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(upload_foto.this);
        builder.setTitle("Ambil Foto");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Ambil Foto")) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = Uri.fromFile(getOutputPhotoFile());
                    i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(i, CAPTURE_IMAGE_ACTIVITY_REQ);
                } else if (options[item].equals("Pilih dari Galeri")) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    );
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                } else if (options[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQ) {
                    if (resultCode == RESULT_OK) {
                        Uri photoUri = null;
                        if (data == null) {
                            // A known bug here! The image should have saved in fileUri
                            Toast.makeText(this, "Image saved successfully",
                                    Toast.LENGTH_LONG).show();
                            photoUri = fileUri;

                        } else {
                            photoUri = data.getData();
                            Toast.makeText(this, "Image saved successfully in: " + data.getData(),
                                    Toast.LENGTH_LONG).show();
                        }
                        mark = 1;
                        showPhoto(photoUri.getPath());

                    } else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Callout for image capture failed!",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case 1:
                // When an Image is picked
                if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                        && null != data) {
                    // Get the Image from data

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    ImageView imgView = (ImageView) findViewById(R.id.gambar);
                    // Set the Image in ImageView after decoding the String
                    imgView.setImageBitmap(BitmapFactory
                            .decodeFile(imgDecodableString));

                } else {
                    Toast.makeText(this, "Anda belum memilih foto",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



    private void showPhoto(String photoUri) {
        File imageFile = new File(photoUri);
        if (imageFile.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            photoUriUrl = imageFile.getAbsolutePath();
            BitmapDrawable drawable = new BitmapDrawable(this.getResources(), bitmap);
            gambar.setScaleType(ImageView.ScaleType.FIT_CENTER);
            gambar.setImageDrawable(drawable);
        }
    }

    private  void insertFotoSeminar(){

        File photo;
        if(mark == 0)
        {
            photo = new File(imgDecodableString);
        }
        else
        {
            photo = new File(photoUriUrl);
        }

        TypedFile typedImage = new TypedFile("image/*", photo);

        ////Here we will handle the http request to insert user to mysql db
        //creating a restadapter

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);


        //defining the method inertuser of user interface
        api.insertFotoSeminar(
                //passing the value by getting it from edit text

                Id,
                typedImage,
                //creating an anonymous callback

                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object

                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Displaying the output as a toast
                        Toast.makeText(upload_foto.this, "Data berhasil diinput", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(upload_foto.this, "Data gagal diinput", Toast.LENGTH_LONG).show();

                    }
                }

        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}