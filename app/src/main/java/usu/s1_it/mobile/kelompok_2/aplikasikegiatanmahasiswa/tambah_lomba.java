package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class tambah_lomba extends AppCompatActivity implements View.OnClickListener{

    private static int RESULT_LOAD_IMG = 2;
    String imgDecodableString;

    private EditText editTextmulai;
    private EditText editTextselesai;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    private EditText editTextnama;
    private EditText editTexttema;
    private EditText editTextjenis;
    private EditText editTextjadwal;
    private EditText editTextbiaya;
    private EditText editTextlink;
    private EditText editTextlokasi;
    private EditText editTextcp;

    private EditText editTextpenyelenggara;
    private Button buttonsubmit;

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/insert_lomba.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_lomba);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        findViewsById();

        setDateTimeField();

        editTextnama =(EditText) findViewById(R.id.txtnama);
        editTexttema =(EditText) findViewById(R.id.txttema);
        editTextjenis =(EditText) findViewById(R.id.jns_lomba2);
        editTextjadwal =(EditText) findViewById(R.id.txtjdwl_daftar);
        editTextmulai =(EditText) findViewById(R.id.txtjdwl_mulai);
        editTextselesai =(EditText) findViewById(R.id.txtjdwl_selesai);
        editTextbiaya =(EditText) findViewById(R.id.txtbiaya);
        editTextlink =(EditText) findViewById(R.id.txtlink);
        editTextlokasi =(EditText) findViewById(R.id.txtlokasi);
        editTextcp =(EditText) findViewById(R.id.editTextcp);
        editTextpenyelenggara =(EditText) findViewById(R.id.txtpenyelenggara_lomba);

        //add listener button
        buttonsubmit = (Button) findViewById(R.id.btnTambahLomba);

        buttonsubmit.setOnClickListener(this);


    }

    private void findViewsById() {
        editTextmulai = (EditText) findViewById(R.id.txtjdwl_mulai);
        editTextmulai.setInputType(InputType.TYPE_NULL);
        editTextmulai.requestFocus();

        editTextselesai = (EditText) findViewById(R.id.txtjdwl_selesai);
        editTextselesai.setInputType(InputType.TYPE_NULL);
    }

    private void setDateTimeField() {
        editTextmulai.setOnClickListener(this);
        editTextselesai.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextmulai.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextselesai.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    public void onClickUploadFoto (View view)
    {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.brosur_lomba);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));

            }

            else {
                Toast.makeText(this, "Anda belum memilih foto",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }

    private  void insertLomba(){

        File photo = new File(imgDecodableString);
        TypedFile typedImage = new TypedFile("image/*", photo);

        ////Here we will handle the http request to insert user to mysql db
        //creating a restadapter

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);

        //tamrin

        //textname =  editTextName.getText().toString();

        //defining the method inertuser of user interface
        api.insertLomba(
                //passing the value by getting it from edit text

                editTextnama.getText().toString(),
                editTexttema.getText().toString(),
                editTextjenis.getText().toString(),
                editTextjadwal.getText().toString(),
                editTextmulai.getText().toString(),
                editTextselesai.getText().toString(),
                editTextbiaya.getText().toString(),
                editTextlink.getText().toString(),
                editTextlokasi.getText().toString(),
                typedImage,
                editTextcp.getText().toString(),
                editTextpenyelenggara.getText().toString(),
                //creating an anonymous callback

                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object

                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Displaying the output as a toast
                        Toast.makeText(tambah_lomba.this, "Data berhasil diinput", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(tambah_lomba.this, "Data gagal diinput", Toast.LENGTH_LONG).show();

                    }
                }

        );
    }

    //overiding onclick method


    @Override
    public void onClick(View v) {
        if(v == editTextmulai) {
            fromDatePickerDialog.show();
        } else if(v == editTextselesai) {
            toDatePickerDialog.show();
        }else if(v == buttonsubmit)
        {
            insertLomba();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent m=new Intent(tambah_lomba.this,adminbiasa_activity.class);
        m.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(m);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tambah_lomba, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }
}
