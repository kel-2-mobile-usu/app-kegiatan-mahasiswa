package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/4/2016.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class datalomba {

    @SerializedName("data_lomba")
    @Expose
    public List<data_lomba> dataLomba = new ArrayList<data_lomba>();

    /**
     *
     * @return
     * The dataLomba
     */
    public List<data_lomba> getDataLomba() {
        return dataLomba;
    }

    /**
     *
     * @param dataLomba
     * The data_lomba
     */
    public void setDataLomba(List<data_lomba> dataLomba) {
        this.dataLomba = dataLomba;
    }

}
