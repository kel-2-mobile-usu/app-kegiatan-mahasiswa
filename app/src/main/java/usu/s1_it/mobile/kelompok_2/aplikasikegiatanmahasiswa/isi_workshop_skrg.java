package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class isi_workshop_skrg extends AppCompatActivity {
    String PATH = "http://ukm-mobile.esy.es/images/seminar/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_workshop_skrg);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView Header = (TextView) findViewById(R.id.headerws);
        ImageView Gambar = (ImageView) findViewById(R.id.imageWorkshops);
        TextView Tema = (TextView) findViewById(R.id.texttemaw2);
        TextView Tanggal = (TextView) findViewById(R.id.texttglw2);
        TextView Jam = (TextView) findViewById(R.id.textjamw2);
        TextView Lokasi = (TextView) findViewById(R.id.textlokasiw2);
        TextView Pembicara = (TextView) findViewById(R.id.textpembicaraw2);
        TextView Link = (TextView) findViewById(R.id.textlinkw2);
        TextView Biaya = (TextView) findViewById(R.id.textbiayaw2);
        TextView Jenis = (TextView) findViewById(R.id.textjenisw2);
        TextView Contact = (TextView) findViewById(R.id.textcpw2);
        Intent intent = getIntent();

        Header.setText(intent.getStringExtra("Judul"));
        Picasso.with(this).load(PATH + intent.getStringExtra("Gambar")).into(Gambar);
        Tema.setText(intent.getStringExtra("Tema"));
        Tanggal.setText(intent.getStringExtra("Tanggal"));
        Jam.setText(intent.getStringExtra("Jam"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Pembicara.setText(intent.getStringExtra("Pembicara"));
        Link.setText(intent.getStringExtra("Link"));
        Biaya.setText(intent.getStringExtra("Biaya"));
        Jenis.setText(intent.getStringExtra("Jenis"));
        Contact.setText(intent.getStringExtra("Contact Person"));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_isi_workshop_skrg, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
