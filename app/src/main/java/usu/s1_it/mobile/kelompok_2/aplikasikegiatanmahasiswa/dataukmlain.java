package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import java.util.ArrayList;
        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class dataukmlain {

    @SerializedName("data_ukmlain")
    @Expose
    private List<data_ukmlain> dataUkmlain = new ArrayList<data_ukmlain>();

    /**
     *
     * @return
     * The dataUkmlain
     */
    public List<data_ukmlain> getDataUkmlain() {
        return dataUkmlain;
    }

    /**
     *
     * @param dataUkmlain
     * The data_ukmlain
     */
    public void setDataUkmlain(List<data_ukmlain> dataUkmlain) {
        this.dataUkmlain = dataUkmlain;
    }

}