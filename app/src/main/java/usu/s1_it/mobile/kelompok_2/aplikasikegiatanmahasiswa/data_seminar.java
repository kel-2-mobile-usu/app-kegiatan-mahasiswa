package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/4/2016.
 */



        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


public class data_seminar {

    @SerializedName("id_seminar")
    @Expose
    private String idSeminar;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("tema")
    @Expose
    private String tema;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("waktu")
    @Expose
    private String waktu;
    @SerializedName("lokasi")
    @Expose
    private String lokasi;
    @SerializedName("pembicara")
    @Expose
    private String pembicara;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("biaya")
    @Expose
    private String biaya;
    @SerializedName("jenis")
    @Expose
    private String jenis;
    @SerializedName("spanduk")
    @Expose
    private String spanduk;
    @SerializedName("cp")
    @Expose
    private String cp;
    @SerializedName("penyelenggara")
    @Expose
    private String penyelenggara;

    /**
     *
     * @return
     * The idSeminar
     */
    public String getIdSeminar() {
        return idSeminar;
    }

    /**
     *
     * @param idSeminar
     * The id_seminar
     */
    public void setIdSeminar(String idSeminar) {
        this.idSeminar = idSeminar;
    }

    /**
     *
     * @return
     * The judul
     */
    public String getJudul() {
        return judul;
    }

    /**
     *
     * @param judul
     * The judul
     */
    public void setJudul(String judul) {
        this.judul = judul;
    }

    /**
     *
     * @return
     * The tema
     */
    public String getTema() {
        return tema;
    }

    /**
     *
     * @param tema
     * The tema
     */
    public void setTema(String tema) {
        this.tema = tema;
    }

    /**
     *
     * @return
     * The tanggal
     */
    public String getTanggal() {
        return tanggal;
    }

    /**
     *
     * @param tanggal
     * The tanggal
     */
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    /**
     *
     * @return
     * The waktu
     */
    public String getWaktu() {
        return waktu;
    }

    /**
     *
     * @param waktu
     * The waktu
     */
    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    /**
     *
     * @return
     * The lokasi
     */
    public String getLokasi() {
        return lokasi;
    }

    /**
     *
     * @param lokasi
     * The lokasi
     */
    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    /**
     *
     * @return
     * The pembicara
     */
    public String getPembicara() {
        return pembicara;
    }

    /**
     *
     * @param pembicara
     * The pembicara
     */
    public void setPembicara(String pembicara) {
        this.pembicara = pembicara;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The biaya
     */
    public String getBiaya() {
        return biaya;
    }

    /**
     *
     * @param biaya
     * The biaya
     */
    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    /**
     *
     * @return
     * The jenis
     */
    public String getJenis() {
        return jenis;
    }

    /**
     *
     * @param jenis
     * The jenis
     */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    /**
     *
     * @return
     * The spanduk
     */
    public String getSpanduk() {
        return spanduk;
    }

    /**
     *
     * @param spanduk
     * The spanduk
     */
    public void setSpanduk(String spanduk) {
        this.spanduk = spanduk;
    }



    /**
     *
     * @return
     * The cp
     */
    public String getCp() {
        return cp;
    }

    /**
     *
     * @param cp
     * The cp
     */
    public void setCp(String cp) {
        this.cp = cp;
    }





    /**
     *
     * @return
     * The penyelenggara
     */
    public String getPenyelenggara() {
        return penyelenggara;
    }

    /**
     *
     * @param penyelenggara
     * The penyelenggara
     */
    public void setPenyelenggara(String penyelenggara) {
        this.penyelenggara = penyelenggara;
    }

}