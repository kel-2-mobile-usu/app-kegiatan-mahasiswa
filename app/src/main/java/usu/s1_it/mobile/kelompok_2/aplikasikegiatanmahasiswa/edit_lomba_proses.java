package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class edit_lomba_proses extends AppCompatActivity implements View.OnClickListener{

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/edit_lomba.php";

    private TextView ID;
    private EditText Judul;
    private EditText Tema;
    private EditText Jenis;
    private EditText Jadwal;
    private EditText Mulai;
    private EditText Selesai;
    private EditText Biaya;
    private EditText Link;
    private EditText Lokasi;
    private EditText Contact;
    private EditText Penyelenggara;

    private Button buttonsubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_lomba_proses);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ID = (TextView) findViewById(R.id.txtid);
        Judul = (EditText) findViewById(R.id.txtjudul);
        Tema = (EditText) findViewById(R.id.txttema);
        Jenis = (EditText) findViewById(R.id.txtjenis);
        Jadwal = (EditText) findViewById(R.id.txtjadwal);
        Mulai = (EditText) findViewById(R.id.txttglmulai);
        Selesai = (EditText) findViewById(R.id.txttglselesai);
        Biaya = (EditText) findViewById(R.id.txtbiaya);
        Link = (EditText) findViewById(R.id.txtlink);
        Lokasi = (EditText) findViewById(R.id.txtlokasi);
        Contact = (EditText) findViewById(R.id.txtcp);
        Penyelenggara = (EditText) findViewById(R.id.txtpenyelenggara);

        Intent intent = getIntent();
        ID.setText(intent.getStringExtra("ID"));
        Judul.setText(intent.getStringExtra("Judul"));
        Tema.setText(intent.getStringExtra("Tema"));
        Jenis.setText(intent.getStringExtra("Jenis"));
        Jadwal.setText(intent.getStringExtra("Jadwal"));
        Mulai.setText(intent.getStringExtra("Mulai"));
        Selesai.setText(intent.getStringExtra("Selesai"));
        Biaya.setText(intent.getStringExtra("Biaya"));
        Link.setText(intent.getStringExtra("Link"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Contact.setText(intent.getStringExtra("Contact Person"));
        Penyelenggara.setText(intent.getStringExtra("Penyelenggara"));

        buttonsubmit = (Button) findViewById(R.id.updatelomba);
        buttonsubmit.setOnClickListener(this);
    }


    private  void editLomba(){


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);

        //tamrin

        //textname =  editTextName.getText().toString();

        //defining the method inertuser of user interface
        api.editLomba(
                //passing the value by getting it from edit text

                ID.getText().toString(),
                Judul.getText().toString(),
                Tema.getText().toString(),
                Jenis.getText().toString(),
                Jadwal.getText().toString(),
                Mulai.getText().toString(),
                Selesai.getText().toString(),
                Biaya.getText().toString(),
                Link.getText().toString(),
                Lokasi.getText().toString(),
                Contact.getText().toString(),
                Penyelenggara.getText().toString(),
                //creating an anonymous callback

                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object

                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Displaying the output as a toast
                        Toast.makeText(edit_lomba_proses.this, "Data berhasil diubah", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(edit_lomba_proses.this, "Data gagal diubah", Toast.LENGTH_LONG).show();

                    }
                }

        );
    }


    @Override
    public void onClick(View v) {
        if(v == buttonsubmit)
        {
            editLomba();
        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_lomba_proses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
